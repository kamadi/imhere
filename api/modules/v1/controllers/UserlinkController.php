<?php
/**
 * Created by PhpStorm.
 * User: Madiyar
 * Date: 21.12.2015
 * Time: 11:03
 */

namespace api\modules\v1\controllers;


use api\modules\v1\models\Info;
use api\modules\v1\models\Link;
use api\modules\v1\models\UserInfo;
use api\modules\v1\models\UserLink;
use app\models\User;
use Yii;
use yii\db\Exception;
use yii\helpers\Json;
use yii\web\ServerErrorHttpException;

class UserlinkController extends BaseController
{
    public $modelClass = 'api\modules\v1\models\UserLink';


    public function actions()
    {
        $actions = parent::actions();
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['view']);
        return $actions;
    }

    public function actionCreate()
    {
        $array = Json::decode($_POST['link']);
        $user_id = $_POST['user_id'];
        $allLink = Link::find()->all();
        foreach ($array as $key => $value) {
            foreach ($allLink as $link) {
                if ($link->name == $key) {
                    $user_link = new UserLink();
                    $user_link->link_id = $link->name;
                    $user_link->value = $value;
                    $user_link->user_id = $user_id;

                    try {
                        $user_link->save();
                    } catch (Exception $e) {
                        throw new ServerErrorHttpException('Duplicated user link');
                    }

                }
            }
        }
        $user_link = Yii::$app->db->createCommand('SELECT * FROM user_link WHERE user_id=:user_id')
            ->bindValue(':user_id', $user_id)
            ->queryAll();
        return $user_link;
    }

    public function actionUpdate($id)
    {
        $array = Json::decode(Yii::$app->request->getBodyParam('link'));

        $userLinks = UserLink::find()->where(['user_id' => $id])->all();
        foreach ($array as $key => $value) {
            foreach ($userLinks as $userLink) {
                if ($userLink->link_id == $key) {
                    $userLink->value = $value;
                    try {
                        $userLink->save();
                    } catch (Exception $e) {
                        throw new ServerErrorHttpException('Unknown error');
                    }

                }
            }
        }

        $user_link = Yii::$app->db->createCommand('SELECT * FROM user_link WHERE user_id=:user_id')
            ->bindValue(':user_id', $id)
            ->queryAll();
        return $user_link;
    }

    public function actionView($id)
    {
        return $user_link = Yii::$app->db->createCommand('SELECT * FROM user_link WHERE user_id=:user_id')
            ->bindValue(':user_id', $id)
            ->queryAll();
    }
}