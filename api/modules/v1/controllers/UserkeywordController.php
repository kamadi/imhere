<?php
/**
 * Created by PhpStorm.
 * User: Madiyar
 * Date: 21.12.2015
 * Time: 11:27
 */

namespace api\modules\v1\controllers;


use api\modules\v1\models\Keyword;
use api\modules\v1\models\UserKeyword;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Exception;
use yii\helpers\Json;
use yii\web\ServerErrorHttpException;

class UserkeywordController extends BaseController
{
    public $modelClass = 'api\modules\v1\models\UserKeyword';


    public function actions()
    {
        $actions = parent::actions();
        unset($actions['create']);
        $actions['index']=[
            'class' => 'yii\rest\IndexAction',
            'modelClass' => $this->modelClass,
            'checkAccess' => [$this, 'checkAccess'],
            'prepareDataProvider' => function ($action) {
                /* @var $model UserKeyword */
                $model = new $this->modelClass;
                $query = $model::find();
                $dataProvider = new ActiveDataProvider([
                    'query' => $query,
                    'pagination' => [
                        'pageSize' => 25,
                    ],
                ]);
                $model->setAttribute('user_id', @$_GET['user_id']);
                $query->andFilterWhere(['=', 'user_id', $model->user_id]);

                return $dataProvider;
            }
        ];
        return $actions;
    }

    public function actionCreate()
    {
        $keywords = Json::decode($_POST['keywords']);
        $user_id = $_POST['user_id'];

        foreach ($keywords as $userKeyword) {
            $keyword = Keyword::find()->where(['name' => $userKeyword])->one();
            if ($keyword) {
                UserKeyword::create($keyword->id, $user_id);
            } else {
                $keyword = new Keyword();
                $keyword->name = $userKeyword;
                if ($keyword->save()) {
                    UserKeyword::create($keyword->id, $user_id);
                }
            }
        }
        $user_keyword = Yii::$app->db->createCommand('SELECT * FROM user_keyword WHERE user_id=:user_id')
            ->bindValue(':user_id', $user_id)
            ->queryAll();
        return $user_keyword;

    }

}