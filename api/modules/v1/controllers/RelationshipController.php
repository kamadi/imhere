<?php
/**
 * Created by PhpStorm.
 * User: Madiyar
 * Date: 25.01.2016
 * Time: 17:03
 */

namespace api\modules\v1\controllers;


use Yii;

class RelationshipController extends BaseController
{
    public $modelClass = 'api\modules\v1\models\Relationship';


    public function actionRemove()
    {
        $request = Yii::$app->request;
        $user_id = $request->post('user_id');
        $card_id = $request->post('card_id');
        if (Yii::$app->db->createCommand()->delete('relationship', ['user_id' => $user_id, 'card_id' => $card_id])->execute()) {
            Yii::$app->getResponse()->setStatusCode(204);
        }
    }
}