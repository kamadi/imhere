<?php
/**
 * Created by PhpStorm.
 * User: Madiyar
 * Date: 21.12.2015
 * Time: 16:31
 */

namespace api\modules\v1\controllers;


use api\modules\v1\models\States;
use yii\data\ActiveDataProvider;
use yii\rest\ActiveController;

class StateController extends ActiveController
{
    public $modelClass = 'api\modules\v1\models\States';

    public function actions()
    {
        $actions = parent::actions();
        $actions['create'] = null;
        $actions['update'] = null;
        $actions['delete'] = null;
        $actions['index'] = [
            'class' => 'yii\rest\IndexAction',
            'modelClass' => $this->modelClass,
            'checkAccess' => [$this, 'checkAccess'],
            'prepareDataProvider' => function ($action) {
                /* @var $model States */
                $model = new $this->modelClass;
                $query = $model::find();
                $dataProvider = new ActiveDataProvider([
                    'query' => $query,
                    'pagination' => false
                ]);
                $model->setAttribute('countryID', @$_GET['country_id']);
                $query->andFilterWhere(['=', 'countryID', $model->countryID]);
                return $dataProvider;
            }
        ];
        return $actions;
    }


}