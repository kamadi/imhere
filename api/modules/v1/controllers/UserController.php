<?php
/**
 * Created by PhpStorm.
 * User: Madiyar
 * Date: 12.11.2015
 * Time: 11:44
 */

namespace api\modules\v1\controllers;


use api\modules\v1\models\Info;
use api\modules\v1\models\User;
use api\modules\v1\models\UserInfo;
use Yii;
use yii\base\Exception;
use yii\data\ActiveDataProvider;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\rest\ActiveController;
use yii\web\ServerErrorHttpException;

class UserController extends BaseController
{

    public $modelClass = 'api\modules\v1\models\User';

    public function actions()
    {
        $actions = parent::actions();
//        unset($actions['create']);
        $actions['index']=[
            'class' => 'yii\rest\IndexAction',
            'modelClass' => $this->modelClass,
            'checkAccess' => [$this, 'checkAccess'],
            'prepareDataProvider' => function ($action) {
                /* @var $model User */
                $model = new $this->modelClass;
                $query = $model::find();
                $dataProvider = new ActiveDataProvider([
                    'query' => $query,
                    'pagination' => [
                        'pageSize' => 25,
                    ],
                ]);

                return $dataProvider;
            }
        ];
        return $actions;
    }

    public function actionSearch($query, $cityID=0,$page=0)
    {
        return User::search($query, $cityID,$page);
    }

    public function actionImage()
    {
        $dir = Yii::getAlias('@webroot') .User::IMAGE_PATH;
        $file_type = strtolower($_FILES['file']['type']);
        if ($file_type == 'image/png' || $file_type == 'image/jpg' || $file_type == 'image/gif' || $file_type == 'image/jpeg' || $file_type == 'image/pjpeg') {
            try {
                $user = User::findOne($_POST['user_id']);

                $extension = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);

                $filename = 'img_' . $user->id . '.' . $extension;

                $dir .= "/" . $user->id . "/";

                FileHelper::createDirectory($dir);
                $file = $dir . $filename;

                move_uploaded_file($_FILES['file']['tmp_name'], $file);


                $user->image = $filename;

                if ($user->save()) {
                    return $user;
                } else {
                    throw new ServerErrorHttpException('Failed to save the user image for unknown reason');
                }
            } catch (Exception $e) {
                throw new ServerErrorHttpException('Failed to create the user image for unknown reason');
            }
        } else {
            throw new ServerErrorHttpException("not allowed image format");
        }
    }



}