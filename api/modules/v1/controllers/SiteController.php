<?php
/**
 * Created by PhpStorm.
 * User: Madiyar
 * Date: 02.04.2015
 * Time: 11:57
 */

namespace api\modules\v1\controllers;
use api\modules\v1\models\User;
use common\models\LoginForm;
use Yii;
use yii\db\Exception;
use yii\filters\RateLimiter;
use yii\helpers\Url;
use yii\rest\ActiveController;
use yii\web\ServerErrorHttpException;

class SiteController extends ActiveController
{
    public $modelClass = 'api\modules\v1\models\User';

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['rateLimiter'] = [
            'class' => RateLimiter::className(),
            'enableRateLimitHeaders' => false,
        ];

        $behaviors['verbs'] = [
            'class' => \yii\filters\VerbFilter::className(),
            'actions' => [
                'login' => ['POST', 'OPTIONS'],
            ],
        ];

        return $behaviors;
    }

    public function actionLogin()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        if (\Yii::$app->getRequest()->getMethod() === 'OPTIONS') {
            \Yii::$app->getResponse()->getHeaders()->set('Allow', 'POST');
        } else {
            $model = new LoginForm();
            if ($model->load(\Yii::$app->getRequest()->getBodyParams(), '') && $model->login()) {
                $arr = [];
                $arr['access-token'] = \Yii::$app->user->identity->getAuthKey();
                $arr['id'] = \Yii::$app->user->identity->getId();
                return $arr;
            } else {
                $model->validate();
                return $model;
            }
        }
    }


    public function actionRegister()
    {
        $email = $_POST['email'];
        $password = $_POST['password'];
        $username = $_POST['username'];
        $user = new User();
        $user->email = $email;
        $user->username = $username;
        $user->setPassword($password);
        $user->generateAuthKey();
        $user->generatePasswordResetToken();
        try {
            if ($user->save()) {
                $response = Yii::$app->getResponse();
                $response->setStatusCode(201);
            } elseif (!$user->hasErrors()) {
                throw new ServerErrorHttpException('Failed to create the user.User is not valid.');
            }
        } catch (Exception $e) {
            throw new ServerErrorHttpException('Failed to create the user.User is not valid.');
        }
        $arr  = [];
        $arr['id']=$user->id;
        $arr['username']=$user->username;
        $arr['email']=$user->email;
        $arr['access-token']=$user->auth_key;
        return $arr;
    }

    public function actionSearch($query, $cityID=0,$page=0)
    {
        return User::search($query, $cityID,$page);
    }
}
