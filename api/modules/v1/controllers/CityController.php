<?php
/**
 * Created by PhpStorm.
 * User: Madiyar
 * Date: 21.12.2015
 * Time: 16:33
 */

namespace api\modules\v1\controllers;


use api\modules\v1\models\Cities;
use yii\data\ActiveDataProvider;
use yii\rest\ActiveController;

class CityController extends ActiveController
{
    public $modelClass = 'api\modules\v1\models\Cities';

    public function actions()
    {
        $actions = parent::actions();
        $actions['create'] = null;
        $actions['update'] = null;
        $actions['delete'] = null;
        $actions['index'] = [
            'class' => 'yii\rest\IndexAction',
            'modelClass' => $this->modelClass,
            'checkAccess' => [$this, 'checkAccess'],
            'prepareDataProvider' => function ($action) {
                /* @var $model Cities */
                $model = new $this->modelClass;
                $query = $model::find();
                $dataProvider = new ActiveDataProvider([
                    'query' => $query,
                    'pagination' => false
                ]);
                $model->setAttribute('stateID', @$_GET['state_id']);
                $query->andFilterWhere(['=', 'stateID', $model->stateID]);

                $model->setAttribute('countryID', @$_GET['country_id']);
                $query->andFilterWhere(['=', 'countryID', $model->countryID]);

                return $dataProvider;
            }
        ];
        return $actions;
    }
}