<?php
/**
 * Created by PhpStorm.
 * User: Madiyar
 * Date: 12.11.2015
 * Time: 11:41
 */

namespace api\modules\v1\controllers;

use yii\filters\auth\QueryParamAuth;
use yii\filters\ContentNegotiator;
use yii\rest\ActiveController;
use yii\web\Response;

class BaseController extends ActiveController{

    public function behaviors(){
        $behaviors = parent::behaviors();

        $behaviors['bootstrap'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];

        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
        ];
        return $behaviors;
    }

}