<?php
namespace api\modules\v1;

/**
 * Created by PhpStorm.
 * User: Madiyar
 * Date: 12.11.2015
 * Time: 11:37
 */


class Module extends \yii\base\Module {

    public $controllerNamespace = 'api\modules\v1\controllers';

    public function init()
    {
        parent::init();
    }

}