<?php

namespace api\modules\v1\models;

use Yii;

/**
 * This is the model class for table "keyword".
 *
 * @property string $id
 * @property string $name
 *
 * @property UserKeyword[] $userKeywords
 * @property User[] $users
 */
class Keyword extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'keyword';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    public function fields()
    {
        return ['id','name'];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserKeywords()
    {
        return $this->hasMany(UserKeyword::className(), ['keyword_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])->viaTable('user_keyword', ['keyword_id' => 'id']);
    }
}
