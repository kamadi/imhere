<?php

namespace api\modules\v1\models;

use Yii;

/**
 * This is the model class for table "user_link".
 *
 * @property integer $user_id
 * @property string $link_id
 * @property string $value
 *
 * @property Link $link
 * @property User $user
 */
class UserLink extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_link';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'link_id', 'value'], 'required'],
            [['user_id'], 'integer'],
            [['link_id'], 'string', 'max' => 20],
            [['value'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'link_id' => 'Link ID',
            'value' => 'Value',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLink()
    {
        return $this->hasOne(Link::className(), ['name' => 'link_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
