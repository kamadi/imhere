<?php

namespace api\modules\v1\models;

use Yii;

/**
 * This is the model class for table "cities".
 *
 * @property string $cityID
 * @property string $cityName
 * @property integer $stateID
 * @property string $countryID
 * @property double $latitude
 * @property double $longitude
 */
class Cities extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cities';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cityName'], 'required'],
            [['stateID'], 'integer'],
            [['latitude', 'longitude'], 'number'],
            [['cityName'], 'string', 'max' => 50],
            [['countryID'], 'string', 'max' => 3]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cityID' => 'City ID',
            'cityName' => 'City Name',
            'stateID' => 'State ID',
            'countryID' => 'Country ID',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
        ];
    }
}
