<?php

namespace api\modules\v1\models;

use Yii;

/**
 * This is the model class for table "states".
 *
 * @property integer $stateID
 * @property string $stateName
 * @property string $countryID
 * @property double $latitude
 * @property double $longitude
 */
class States extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'states';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['countryID'], 'required'],
            [['latitude', 'longitude'], 'number'],
            [['stateName'], 'string', 'max' => 50],
            [['countryID'], 'string', 'max' => 3]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'stateID' => 'State ID',
            'stateName' => 'State Name',
            'countryID' => 'Country ID',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
        ];
    }
}
