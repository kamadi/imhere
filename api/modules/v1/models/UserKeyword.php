<?php

namespace api\modules\v1\models;

use Yii;
use yii\db\Exception;
use yii\web\ServerErrorHttpException;

/**
 * This is the model class for table "user_keyword".
 *
 * @property integer $user_id
 * @property string $keyword_id
 *
 * @property Keyword $keyword
 * @property User $user
 */
class UserKeyword extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_keyword';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'keyword_id'], 'required'],
            [['user_id', 'keyword_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'keyword_id' => 'Keyword ID',
        ];
    }

    public function fields()
    {
        return ['user_id','keyword'];
    }

    public function extraFields(){
        return ['user'];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKeyword()
    {
        return $this->hasOne(Keyword::className(), ['id' => 'keyword_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public static function create($keyword_id, $user_id)
    {
        $newUserKeyword = new UserKeyword();
        $newUserKeyword->keyword_id = $keyword_id;
        $newUserKeyword->user_id = $user_id;
        try {
            $newUserKeyword->save();
        } catch (Exception $e) {
            throw new ServerErrorHttpException('Failed to create the user keyword for unknown reason');
        }
    }
}
