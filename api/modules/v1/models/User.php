<?php
/**
 * Created by PhpStorm.
 * User: Madiyar
 * Date: 12.11.2015
 * Time: 11:38
 */

namespace api\modules\v1\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $phone
 * @property string $name
 * @property string $surname
 * @property integer $city_id
 * @property string $address
 * @property double $rating
 * @property string $company
 * @property string $image
 * @property string $job
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Relationship[] $relationships
 * @property Relationship[] $relationships0
 * @property Cities $city
 * @property UserLink[] $userLinks
 * @property Link[] $links
 * @property UserKeyword[] $userKeywords
 * @property Keyword[] $keywords
 */
class User extends \common\models\User
{
    const IMAGE_PATH = "/images/users";

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['auth_key', 'password_hash', 'email'], 'required'],
            [['city_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['rating'], 'number'],
            [['username', 'password_hash', 'password_reset_token', 'email', 'phone', 'name', 'surname', 'address', 'image', 'description1', 'description2', 'description3', 'description4'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['email'], 'unique'],
            [['username'], 'unique'],
            [['password_reset_token'], 'unique']
        ];
    }

    public function behaviours()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ]
            ]
        ];
    }

    public function fields()
    {
        return ['id', 'username', 'email', 'name', 'surname', 'address', 'image', 'rating', 'city_id','phone','description1','description2','description3','description4',];
    }


    public static function search($query, $cityID = 0, $page = 0)
    {
        $offset = $page * 10;
        $query = preg_replace('!\s+!', ' ', $query);
        $query = str_replace(' ', '|', $query);

        $sql = 'SELECT DISTINCT u.id,u.email,u.name,u.surname,u.phone,u.description1,u.description2,u.description3,u.description4,u.rating
                FROM user u
                INNER JOIN user_keyword uk ON u.id=uk.user_id
                INNER JOIN keyword k ON k.id=uk.keyword_id
                WHERE (k.name REGEXP "' . $query . '"
                OR u.description1 REGEXP "' . $query . '"
                OR u.description2 REGEXP "' . $query . '"
                OR u.description3 REGEXP "' . $query . '"
                OR u.description4 REGEXP "' . $query . '"
                OR u.username REGEXP "' . $query . '"
                OR u.name REGEXP "' . $query . '"
                OR u.surname REGEXP "' . $query . '")  ';

        if ($cityID) {
            $sql .= ' AND u.city_id =:city_id ';
        }

        if ($page) {
            $sql .= ' LIMIT 10 OFFSET :offset ';
        } else {
            $sql .= ' LIMIT 10';
        }

        $query = Yii::$app->db
            ->createCommand($sql);

        if ($cityID) {
            $query->bindValue(':city_id', $cityID);
        }

        if ($page) {
            $query->bindValue(':offset', $offset);
        }
        $users = $query->queryAll();


        $user_ids = [];
        $ids = '';

        for($i=0;$i<count($users);$i++) {
            $user_ids[] = $users[$i]['id'];
            $users[$i]['socials'] = [];
        }

        $ids = implode(", ", $user_ids);;

        $sql = 'SELECT link_id,user_id FROM user_link WHERE user_id IN (:ids)';

        $all_user_info = Yii::$app->db->createCommand($sql)->bindValue(':ids', $ids)->queryAll();

        foreach ($all_user_info as $user_info) {
            for($i=0;$i<count($users);$i++) {

                if ($user_info['user_id'] == $users[$i]['id']) {
                    $users[$i]['socials'][] = $user_info['link_id'];
                }
            }
        }
        return $users;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRelationships()
    {
        return $this->hasMany(Relationship::className(), ['card_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRelationships0()
    {
        return $this->hasMany(Relationship::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(Cities::className(), ['cityID' => 'city_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserLinks()
    {
        return $this->hasMany(UserLink::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLinks()
    {
        return $this->hasMany(Link::className(), ['name' => 'link_id'])->viaTable('user_link', ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserKeywords()
    {
        return $this->hasMany(UserKeyword::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKeywords()
    {
        return $this->hasMany(Keyword::className(), ['id' => 'keyword_id'])->viaTable('user_keyword', ['user_id' => 'id']);
    }
}