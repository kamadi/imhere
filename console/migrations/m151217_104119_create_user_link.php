<?php

use yii\db\Schema;
use yii\db\Migration;

class m151217_104119_create_user_link extends Migration
{
    public function up()
    {
        $this->createTable('{{%user_link}}', [
            'user_id' => $this->integer()->notNull(),
            'link_id' => $this->string(20)->notNull(),
            'value' => $this->string()->notNull(),
            'PRIMARY KEY(user_id, link_id)'
        ]);

        $this->createIndex('idx-user_link-user_id', 'user_link', 'user_id');
        $this->createIndex('idx-user_link-info_id', 'user_link', 'link_id');

        $this->addForeignKey('fk-user_link-user_id', 'user_link', 'user_id', 'user', 'id', 'CASCADE');
        $this->addForeignKey('fk-user_link-info_id', 'user_link', 'link_id', 'link', 'name', 'CASCADE');

    }

    public function down()
    {
        $this->dropTable('user_link');
    }
}
