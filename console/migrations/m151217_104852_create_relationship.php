<?php

use yii\db\Schema;
use yii\db\Migration;

class m151217_104852_create_relationship extends Migration
{
    public function up()
    {
        $this->createTable('{{%relationship}}', [
            'user_id' => $this->integer()->notNull(),
            'card_id' => $this->integer()->notNull(),
            'PRIMARY KEY(user_id, card_id)'
        ]);

        $this->createIndex('idx-relationship-user_id', 'relationship', 'user_id');
        $this->createIndex('idx-relationship-card_id', 'relationship', 'card_id');

        $this->addForeignKey('fk-relationship-user_id', 'relationship', 'user_id', 'user', 'id', 'CASCADE');
        $this->addForeignKey('fk-relationship-card_id', 'relationship', 'card_id', 'user', 'id', 'CASCADE');

    }

    public function down()
    {
        $this->dropTable('relationship');
    }
}
