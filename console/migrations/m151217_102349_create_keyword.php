<?php

use yii\db\Schema;
use yii\db\Migration;

class m151217_102349_create_keyword extends Migration
{
    public function up()
    {
        $this->createTable('{{%keyword}}', [
            'id' => $this->bigPrimaryKey(),
            'name'=>$this->string()->unique()->notNull(),
            'group_id'=>$this->bigInteger()
        ]);

        $this->createIndex('idx-keyword_group-group_id', 'keyword', 'group_id');
        $this->addForeignKey('fk-keyword_group-group_id', 'keyword', 'group_id', 'keyword_group', 'id', 'CASCADE');


    }

    public function down()
    {
        $this->dropTable('{{%keyword}}');
    }


}
