<?php

/**
 * Created by PhpStorm.
 * User: Madiyar
 * Date: 17.12.2015
 * Time: 20:59
 */
class m151217_102347_create_keyword_group extends \yii\db\Migration
{
    public function up()
    {
        $this->createTable('{{%keyword_group}}', [
            'id' => $this->bigPrimaryKey()
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%keyword_group}}');
    }
}