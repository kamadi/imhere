<?php

use yii\db\Schema;
use yii\db\Migration;

class m151217_103900_create_link extends Migration
{
    public function safeUp()
    {
//        $this->createTable('{{%info}}', [
//            'id' => $this->primaryKey(),
//            'name' => $this->string(20)->unique()->notNull()
//        ]);
//
//        $this->insert('{{%info}}', ['name' => 'facebook']);
//        $this->insert('{{%info}}', ['name' => 'twitter']);
//        $this->insert('{{%info}}', ['name' => 'instagram']);
//        $this->insert('{{%info}}', ['name' => 'youtube']);
//        $this->insert('{{%info}}', ['name' => 'presentation']);
//        $this->insert('{{%info}}', ['name' => 'cv']);

        $this->createTable('{{%link}}', [
            'name' => $this->string(20)->notNull()
        ]);

        $this->addPrimaryKey('pk_link', 'link', 'name');

        $this->insert('{{%link}}', ['name' => 'facebook']);
        $this->insert('{{%link}}', ['name' => 'linkedin']);
        $this->insert('{{%link}}', ['name' => 'instagram']);
        $this->insert('{{%link}}', ['name' => 'youtube']);
        $this->insert('{{%link}}', ['name' => 'doc']);
    }


    public function safeDown()
    {
        $this->dropTable('{{%link}}');
    }
}
