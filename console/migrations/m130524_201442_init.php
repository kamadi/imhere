<?php

use yii\db\Schema;
use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),
            'phone' => $this->string(),
            'name' => $this->string(),
            'surname' => $this->string(),
            'city_id' => $this->integer(),
            'address' => $this->string(),
            'rating' => $this->double()->defaultValue(0),
            'image' => $this->string(),
            'description1' => $this->string(),
            'description2' => $this->string(),
            'description3' => $this->string(),
            'description4' => $this->string(),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('idx-user_cities-city_id', 'user', 'city_id');

        $this->addForeignKey('fk-user_cities-city_id', 'user', 'city_id', 'cities', 'cityID', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%user}}');
    }
}
