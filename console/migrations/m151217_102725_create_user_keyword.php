<?php

use yii\db\Schema;
use yii\db\Migration;

class m151217_102725_create_user_keyword extends Migration
{
    public function up()
    {
        $this->createTable('{{%user_keyword}}', [
            'user_id' => $this->integer()->notNull(),
            'keyword_id' => $this->bigInteger()->notNull(),
            'PRIMARY KEY(user_id, keyword_id)'
        ]);

        $this->createIndex('idx-user_keyword-user_id', 'user_keyword', 'user_id');
        $this->createIndex('idx-user_keyword-keyword_id', 'user_keyword', 'keyword_id');

        $this->addForeignKey('fk-user_keyword-user_id', 'user_keyword', 'user_id', 'user', 'id', 'CASCADE');
        $this->addForeignKey('fk-user_keyword-keyword_id', 'user_keyword', 'keyword_id', 'keyword', 'id', 'CASCADE');

    }

    public function down()
    {
        $this->dropTable('user_keyword');
    }

}
