<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $phone
 * @property string $name
 * @property string $surname
 * @property integer $city_id
 * @property string $address
 * @property double $rating
 * @property string $image
 * @property string $description1
 * @property string $description2
 * @property string $description3
 * @property string $description4
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Relationship[] $relationships
 * @property Relationship[] $relationships0
 * @property Cities $city
 * @property UserKeyword[] $userKeywords
 * @property Keyword[] $keywords
 * @property UserLink[] $userLinks
 * @property Link[] $links
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['auth_key', 'password_hash', 'email', 'created_at', 'updated_at'], 'required'],
            [['city_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['rating'], 'number'],
            [['username', 'password_hash', 'password_reset_token', 'email', 'phone', 'name', 'surname', 'address', 'image', 'description1', 'description2', 'description3', 'description4'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['email'], 'unique'],
            [['username'], 'unique'],
            [['password_reset_token'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'phone' => 'Phone',
            'name' => 'Name',
            'surname' => 'Surname',
            'city_id' => 'City ID',
            'address' => 'Address',
            'rating' => 'Rating',
            'image' => 'Image',
            'description1' => 'Description1',
            'description2' => 'Description2',
            'description3' => 'Description3',
            'description4' => 'Description4',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRelationships()
    {
        return $this->hasMany(Relationship::className(), ['card_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRelationships0()
    {
        return $this->hasMany(Relationship::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(Cities::className(), ['cityID' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserKeywords()
    {
        return $this->hasMany(UserKeyword::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKeywords()
    {
        return $this->hasMany(Keyword::className(), ['id' => 'keyword_id'])->viaTable('user_keyword', ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserLinks()
    {
        return $this->hasMany(UserLink::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLinks()
    {
        return $this->hasMany(Link::className(), ['name' => 'link_id'])->viaTable('user_link', ['user_id' => 'id']);
    }
}
