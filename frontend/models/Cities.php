<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cities".
 *
 * @property integer $cityID
 * @property string $cityName
 * @property integer $stateID
 * @property string $countryID
 * @property double $latitude
 * @property double $longitude
 *
 * @property User[] $users
 */
class Cities extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cities';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cityName'], 'required'],
            [['stateID'], 'integer'],
            [['latitude', 'longitude'], 'number'],
            [['cityName'], 'string', 'max' => 50],
            [['countryID'], 'string', 'max' => 3]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cityID' => 'City ID',
            'cityName' => 'City Name',
            'stateID' => 'State ID',
            'countryID' => 'Country ID',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['city_id' => 'cityID']);
    }
}
