<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order".
 *
 * @property integer $id
 * @property string $start_address
 * @property string $end_address
 * @property double $start_address_latitude
 * @property double $start_address_longitude
 * @property double $end_address_latitude
 * @property double $end_address_longitude
 * @property integer $created_at
 * @property integer $started_at
 * @property integer $finished_at
 * @property integer $price
 * @property integer $dispatcher_id
 * @property integer $driver_id
 *
 * @property User $dispatcher
 * @property User $driver
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['start_address', 'end_address', 'price'], 'required'],
            [['start_address', 'end_address'], 'string'],
            [['start_address_latitude', 'start_address_longitude', 'end_address_latitude', 'end_address_longitude'], 'number'],
            [['created_at', 'started_at', 'finished_at', 'price', 'dispatcher_id', 'driver_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'start_address' => 'Start Address',
            'end_address' => 'End Address',
            'start_address_latitude' => 'Start Address Latitude',
            'start_address_longitude' => 'Start Address Longitude',
            'end_address_latitude' => 'End Address Latitude',
            'end_address_longitude' => 'End Address Longitude',
            'created_at' => 'Created At',
            'started_at' => 'Started At',
            'finished_at' => 'Finished At',
            'price' => 'Price',
            'dispatcher_id' => 'Dispatcher ID',
            'driver_id' => 'Driver ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDispatcher()
    {
        return $this->hasOne(User::className(), ['id' => 'dispatcher_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDriver()
    {
        return $this->hasOne(User::className(), ['id' => 'driver_id']);
    }
}
