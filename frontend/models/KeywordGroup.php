<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "keyword_group".
 *
 * @property string $id
 *
 * @property Keyword[] $keywords
 */
class KeywordGroup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'keyword_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKeywords()
    {
        return $this->hasMany(Keyword::className(), ['group_id' => 'id']);
    }
}
