<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "countries".
 *
 * @property string $countryID
 * @property string $countryName
 * @property string $localName
 * @property string $webCode
 * @property string $region
 * @property string $continent
 * @property double $latitude
 * @property double $longitude
 * @property double $surfaceArea
 * @property integer $population
 */
class Countries extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'countries';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['countryID', 'localName', 'webCode', 'region', 'continent'], 'required'],
            [['continent'], 'string'],
            [['latitude', 'longitude', 'surfaceArea'], 'number'],
            [['population'], 'integer'],
            [['countryID'], 'string', 'max' => 3],
            [['countryName'], 'string', 'max' => 52],
            [['localName'], 'string', 'max' => 45],
            [['webCode'], 'string', 'max' => 2],
            [['region'], 'string', 'max' => 26],
            [['webCode'], 'unique'],
            [['countryName'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'countryID' => 'Country ID',
            'countryName' => 'Country Name',
            'localName' => 'Local Name',
            'webCode' => 'Web Code',
            'region' => 'Region',
            'continent' => 'Continent',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'surfaceArea' => 'Surface Area',
            'population' => 'Population',
        ];
    }
}
